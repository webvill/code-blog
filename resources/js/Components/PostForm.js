import React, { useEffect } from "react";
import { useForm, Link } from "@inertiajs/inertia-react";

import ValidationErrors from "./ValidationErrors";
import Input from "./Input";
import Button from "./Button";
import CodeBlock from "./CodeBlock";

export default function PostForm({ allTags, postToUpdate }) {
    const { data, setData, put, post, processing, errors } = useForm(
        postToUpdate
            ? postToUpdate
            : {
                  title: "",
                  description: "",
                  body: "",
                  published: false,
                  tags: [],
              }
    );

    useEffect(() => {
        if (postToUpdate) {
            return setData(
                "tags",
                postToUpdate.tags.map((tag) => tag.id + "")
            );
        }
    }, []);
    const onHandleChange = (e) => {
        if (e.target.type === "checkbox") {
            e.target.checked
                ? setData(e.target.name, data.tags.concat(e.target.value))
                : setData(
                      e.target.name,
                      data.tags.filter((tag) => tag !== e.target.value)
                  );
        } else {
            setData(e.target.name, e.target.value);
        }
    };

    const submit = (e) => {
        e.preventDefault();
        console.log("data", data);

        if (postToUpdate) {
            put(`/posts/${postToUpdate.id}/update`);
        } else {
            post("/posts");
        }
    };

    return (
        <>
            <ValidationErrors errors={errors} />
            <div className="container mt-3">
                <div className="row">
                    <div className="col-md-6">
                        <div className="card">
                            <div className="card-body">
                                <h5 className="card-title">
                                    {postToUpdate
                                        ? "Uppdatera inlägget"
                                        : "Skapa ett nytt inlägg"}
                                </h5>
                                <form onSubmit={submit}>
                                    <div className="mb-3">
                                        <input
                                            type="hidden"
                                            name="_method"
                                            value="PUT"
                                        />

                                        <label
                                            htmlFor="title"
                                            className="form-label"
                                        >
                                            Titel
                                        </label>
                                        <Input
                                            type="text"
                                            name="title"
                                            value={data.title}
                                            className="form-control"
                                            isFocused={true}
                                            handleChange={onHandleChange}
                                            required
                                        />
                                        {errors.title && (
                                            <div className="text-danger">
                                                {errors.title}
                                            </div>
                                        )}
                                    </div>

                                    <div className="mb-3">
                                        <label
                                            htmlFor="description"
                                            className="form-label"
                                        >
                                            Beskrivning
                                        </label>

                                        <textarea
                                            rows="5"
                                            name="description"
                                            value={data.description}
                                            className="form-control"
                                            onChange={(e) => onHandleChange(e)}
                                        ></textarea>
                                        {errors.description && (
                                            <div className="text-danger">
                                                {errors.description}
                                            </div>
                                        )}
                                    </div>
                                    <div className="mb-3">
                                        <label
                                            htmlFor="body"
                                            className="form-label"
                                        >
                                            Innehåll
                                        </label>

                                        <textarea
                                            rows="20"
                                            name="body"
                                            value={data.body}
                                            className="form-control"
                                            onChange={(e) => onHandleChange(e)}
                                            required
                                        ></textarea>
                                        {errors.body && (
                                            <div className="text-danger">
                                                {errors.body}
                                            </div>
                                        )}
                                    </div>
                                    {allTags.length > 0 && (
                                        <div className="row row-cols-4 px-1 mb-3">
                                            {allTags.map((tag) => (
                                                <div
                                                    key={tag.id}
                                                    className="form-check"
                                                >
                                                    <input
                                                        checked={data.tags.includes(
                                                            tag.id + ""
                                                        )}
                                                        className="form-check-input"
                                                        name="tags"
                                                        type="checkbox"
                                                        value={tag.id}
                                                        onChange={(e) =>
                                                            onHandleChange(e)
                                                        }
                                                    />
                                                    <label
                                                        className="form-check-label badge bg-info"
                                                        htmlFor="tags"
                                                    >
                                                        {tag.name}
                                                    </label>
                                                </div>
                                            ))}
                                        </div>
                                    )}
                                    <div className="d-flex justify-content-end">
                                        <Button
                                            className="btn btn-outline-primary me-2"
                                            processing={processing}
                                        >
                                            {postToUpdate
                                                ? "Uppdatera"
                                                : "Skapa"}
                                        </Button>
                                        <Link
                                            className="btn btn-outline-secondary me-2"
                                            href="/"
                                        >
                                            Avbryt
                                        </Link>
                                        {postToUpdate && (
                                            <Link
                                                className="btn btn-danger"
                                                href={`/posts/${postToUpdate.id}`}
                                                method="delete"
                                            >
                                                Ta bort
                                            </Link>
                                        )}
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-6">
                        {data.body !== "" && (
                            <div className="card">
                                <div className="card-body">
                                    <CodeBlock post={data} />
                                </div>
                            </div>
                        )}
                    </div>
                </div>
            </div>
        </>
    );
}

import React from "react";

export default function Button({
    type = "submit",
    className = "",
    processing,
    onClickHandler,
    children,
}) {
    return (
        <button
            onClick={onClickHandler}
            type={type}
            className={`${processing && "opacity-25"} ` + className}
            disabled={processing}
        >
            {children}
        </button>
    );
}

import React from "react";
import ReactMarkdown from "react-markdown";
import { Head, useForm } from "@inertiajs/inertia-react";
import Default from "../../Layouts/Default";
import ValidationErrors from "../../Components/ValidationErrors";
import PostForm from "../../Components/PostForm";

export default function Create(props) {
    /* const { data, setData, post, processing, errors, reset } = useForm({
        title: "",
        body: "",
        published: false,
    });

    const onHandleChange = (event) => {
        setData(
            event.target.name,
            event.target.type === "checkbox"
                ? event.target.checked
                : event.target.value
        );
    };

    const submit = (e) => {
        e.preventDefault();

        post(route("posts"));
    }; */
    return (
        <Default auth={props.auth}>
            <Head title="Skapa inlägg" />
            <PostForm allTags={props.tags} />
        </Default>
    );
}

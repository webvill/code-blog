import React from "react";
import Default from "../Layouts/Default";
import { Head } from "@inertiajs/inertia-react";

export default function About() {
    return (
        <Default>
            <Head title="About" />
            <h1>About us</h1>
        </Default>
    );
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable = [
        'title',
        'body',
        'user_id'
    ];
    use HasFactory;
    public function tags()
    {
        return $this->belongsToMany(Tag::class);
    }

    public function serie()
    {
        return $this->belongsTo(Serie::class);
    }

}

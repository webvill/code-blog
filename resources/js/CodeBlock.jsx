import React from "react";
import { Prism as SyntaxHighlighter } from "react-syntax-highlighter";
import {
    materialDark,
    materialOceanic,
    materialLight,
} from "react-syntax-highlighter/dist/esm/styles/prism";
const CodeBlock = ({ language, value }) => {
    return (
        <SyntaxHighlighter
            language={language}
            style={materialLight}
            customStyle={preStyles}
            wrapLines={true}
            showLineNumbers
        >
            {value}
        </SyntaxHighlighter>
    );
};

const preStyles = {
    fontSize: "0.8rem",
};

export default CodeBlock;

import React from "react";
import { Head } from "@inertiajs/inertia-react";
import Default from "../../Layouts/Default";
import PostForm from "../../Components/PostForm";

export default function Edit( props ) {
    console.log("props", props);
    return (
        <Default auth={props.auth}>
            <Head title="Skapa inlägg" />
            <PostForm
                postToUpdate={props.postToUpdate}
                allTags={props.allTags}
            />
        </Default>
    );
}

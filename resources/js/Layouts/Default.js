import React, { useEffect } from "react";
import Navbar from "../Components/Navbar";

export default function Default({ auth, dark, resetSearch, handleSearch, children }) {
    //console.log("reset", handleSearch);
    return (
        <main>
            <header>
                <Navbar auth={auth} dark={dark} resetSearch={resetSearch} handleSearch={handleSearch} />
            </header>
            <article>{children}</article>
        </main>
    );
}

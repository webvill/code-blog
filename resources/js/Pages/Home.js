import React, { useState, useEffect, useParams } from "react";

import Default from "../Layouts/Default";
import { Head, Link } from "@inertiajs/inertia-react";
import Post from "../Components/Post";
import axios from "axios";
import { Inertia } from "@inertiajs/inertia";

export default function Home({ auth, allPosts, tagFromRequest }) {
    const [posts, setPosts] = useState([]);
    const [searchTerm, setSearchTerm] = useState("");
    const [resetSearch, setResetSearch] = useState(false)
    const [dark, setDark] = useState(false)
    const [id, setId] = useState(window.location.href.split("/")[4] || "");
    const [tags, setTags] = useState([
        ...new Set(
            !tags &&
                allPosts.map((post) => post.tags.map((tag) => tag.name)).flat()
        ),
    ]);
    const [post, setPost] = useState(
        id ? allPosts.find((p) => p.id === parseInt(id)) : allPosts[0]
    );
    const [selectedTag, setSelectedTag] = useState(
        tagFromRequest ? tagFromRequest : "All"
    );

    function orderPosts(posts) {
        posts = posts.map((post) => ({
            ...post,
            tags: post.tags.map((tag) => tag.name),
        }));

        //console.log("posts tags", postsfromtags);
        const orderedByMonths = _.groupBy(posts, function (element) {
            return element.created_at.substring(0, 7);
        });

        const orderedByYears = _.groupBy(orderedByMonths, function (month) {
            return month[0].created_at.substring(0, 4);
        });
        console.log(
            "posts",
            Object.entries(orderedByMonths)
                .reverse()
                .map((month) => month[1].map((item) => item))
        );
        //return;
        setPosts(orderedByMonths);
        //window.history.pushState("", "", `/posts/${allPosts[0].id}`);
    }
    useEffect(() => {
        orderPosts(allPosts);
    }, [allPosts]);

    function selectTag(selectedTag) {
        setResetSearch(true)
        setSearchTerm("")
        if (selectedTag === "" || !selectedTag) {
            const allTagsPosts = allPosts.map((post) => ({
                ...post,
                tags: post.tags.map((tag) => tag.name),
            }));
            orderPosts(allTagsPosts);
            setPost(allTagsPosts[0]);
            setSelectedTag("");
            window.history.pushState("", "", `/posts/${allTagsPosts[0].id}`);
            
        } else {
            setSelectedTag(selectedTag);
            const postsForTag = allPosts
                .map((post) => ({
                    ...post,
                    tags: post.tags.map((tag) => tag.name),
                }))
                .filter((post) => post.tags.includes(selectedTag));
            orderPosts(postsForTag);
            setPost(postsForTag[0]);
            window.history.pushState("", "", `/posts/${postsForTag[0].id}`);
        }
    }

    function changePost(post) {
        window.history.replaceState("", "", `/posts/${post.id}`);
        //Inertia.visit(`/posts/${post.id}`);
        setPost(post);
    }
    function getPost(id) {
        axios.get(`/posts/${id}`).then((data) => setPost(data.data.post));
        window.history.replaceState("", "", `/posts/${id}`);
    }

    function handleSearch(term) {
        setResetSearch(false)
        setSearchTerm(term);
        setSelectedTag("")
        if (term.length > 3) {
            const searchedPosts = allPosts.filter((post) =>
                post.title.toLowerCase().includes(term.toLowerCase())
            );
            orderPosts(searchedPosts);
        } else if (term.length === 0) {
            orderPosts(allPosts);
        } 
    }

    return (
        <Default
            auth={auth}
            dark={dark}
            resetSearch={resetSearch}
            handleSearch={handleSearch}
        >
            <Head title="Welcome" />

            <div className="row mx-1">
                <div className="col-md-3">
                    {searchTerm && (
                        <div
                            className={`p-2 mb-3 ${
                                dark ? "bg-dark text-white " : "bg-white "
                            }`}
                        >
                            Search for - {searchTerm}
                        </div>
                    )}
                    {Object.entries(posts)
                        //.reverse()
                        .map((month, idx) => (
                            <>
                                <h3
                                    key={month[0]}
                                    className={`lead text-center mb-3 bg-primary ${
                                        idx > 0 ? " mt-3" : ""
                                    }`}
                                >
                                    {month[0]}
                                    {selectedTag !== "" && ` - ${selectedTag}`}
                                </h3>
                                <ul className="list-group">
                                    {month[1].map((item) => (
                                        <li
                                            style={{
                                                backgroundColor: "#ebebeb",
                                                cursor: "pointer",
                                            }}
                                            className={
                                                item.id === post.id
                                                    ? "list-group-item fw-bold text-white bg-info px-1"
                                                    : "list-group-item px-1"
                                            }
                                            key={item.id}
                                        >
                                            <a
                                                onClick={
                                                    () =>
                                                        changePost(
                                                            item
                                                        ) /* getPost(item.id) */
                                                }
                                                /* className="nav-link" */
                                            >
                                                {item.title}
                                            </a>
                                        </li>
                                    ))}
                                </ul>
                            </>
                        ))}
                </div>
                <div className="col-md-6">
                    <div
                        className="card shadow"
                        style={{ backgroundColor: "#ebebeb" }}
                    >
                        <div className="card-body">
                            <Post post={post} />
                        </div>
                    </div>
                </div>
                <div className="col-md-2">
                    <div className="card">
                        <div className="card-body">
                            <div className="card-title">
                                <h3 className="lead text-center">Tags</h3>
                                <span
                                    style={{ cursor: "pointer" }}
                                    className="badge bg-success"
                                    onClick={() => selectTag("")}
                                >
                                    All posts
                                </span>
                                {tags.map((tag) => (
                                    <span
                                        style={{ cursor: "pointer" }}
                                        className={
                                            "badge m-2" +
                                            `${
                                                tag === selectedTag
                                                    ? " bg-info"
                                                    : " bg-primary"
                                            }`
                                        }
                                        onClick={() => selectTag(tag)}
                                        key={tag}
                                    >
                                        {tag}
                                    </span>
                                ))}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </Default>
    );
}

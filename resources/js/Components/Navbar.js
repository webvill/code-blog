import React, { useRef } from "react";
import { Link } from "@inertiajs/inertia-react";
import ResponsiveNavLink from "./ResponsiveNavLink";
import NavLink from "./NavLink";

export default function Navbar({ auth, dark, resetSearch, handleSearch }) {
    
    const formRef = useRef();
    if (resetSearch) {
        formRef.current.reset();
    }

    return (
        <nav className={`navbar navbar-expand-lg ${dark ? "navbar-dark bg-dark" : "navbar-light bg-light"}`}>
            <div className="container-fluid">
                <a className="navbar-brand" href="/">
                    CodeBlog
                </a>
                <button
                    className="navbar-toggler"
                    type="button"
                    data-bs-toggle="collapse"
                    data-bs-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent"
                    aria-expanded="false"
                    aria-label="Toggle navigation"
                >
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div
                    className="collapse navbar-collapse"
                    id="navbarSupportedContent"
                >
                    <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                        <NavLink
                            className="nav-link active"
                            aria-current="page"
                            href="/"
                        >
                            Home
                        </NavLink>
                        <NavLink className="nav-link" href="/posts/create">
                            Add Post
                        </NavLink>
                        {auth.user ? (
                            <NavLink method="post" href={route("logout")}>
                                Log Out
                            </NavLink>
                        ) : (
                            <>
                                <NavLink href={route("login")} as="button">
                                    Sign in
                                </NavLink>
                                <NavLink href={route("register")} as="button">
                                    Register
                                </NavLink>
                            </>
                        )}
                    </ul>
                    <form className="d-flex" ref={formRef}>
                        <input
                            className="form-control me-2"
                            type="search"
                            onChange={(e) => handleSearch(e.target.value)}
                            placeholder="Search post titles"
                            aria-label="Search"
                        />
                    </form>
                </div>
            </div>
        </nav>
    );
}

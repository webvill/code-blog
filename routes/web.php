<?php

use App\Http\Controllers\PostController;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/* Route::get('/', function () {
    return Inertia::render('Welcome', [
        'canLogin' => Route::has('login'),
        'canRegister' => Route::has('register'),
        'laravelVersion' => Application::VERSION,
        'phpVersion' => PHP_VERSION,
    ]);
}); */
Route::inertia('/about', 'About');

Route::controller(PostController::class)->group(function () {
    Route::get('/posts', 'index')->name('posts')->middleware(['auth', 'verified']);
    Route::get('/', 'index')->middleware(['auth', 'verified']);
    Route::get('/posts/create', 'create')->name('posts.create')->middleware(['auth', 'verified']);
    Route::get('/posts/{id}', 'show')->name('posts.show')->middleware(['auth', 'verified']);
    Route::get('/posts/{id}/edit', 'edit')->name('posts.edit')->middleware(['auth', 'verified']);
    Route::post('/posts', 'store')->name('posts.store')->middleware(['auth', 'verified']);
    Route::put('/posts/{id}/update', 'update')->name('posts.update')->middleware(['auth', 'verified']);
    Route::delete('/posts/{id}', 'destroy')->name('posts.destroy')->middleware(['auth', 'verified']);
});

Route::get('/welcome', function() {
    return Inertia::render('Welcome');
});

Route::get('/dashboard', function () {
    return Inertia::render('Dashboard');
})->middleware(['auth', 'verified'])->name('dashboard');

require __DIR__.'/auth.php';

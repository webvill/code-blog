<?php

namespace App\Http\Controllers;

use App\Models\Post;
use App\Models\Tag;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Inertia\Inertia;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $posts = Post::with('tags:name');
        if ($request->has('search')) {
            //dd($request->search);
            $posts = $posts->where('title', 'like', "%$request->search%");
        }
        $posts = $posts->orderBy('created_at', 'desc')->get();
        return Inertia::render('Home', ['allPosts' => $posts]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tags = Tag::all();
        return Inertia::render('Post/Create', [
            'tags' => $tags,
            'can' => [
                 'create_post' => Auth::user()->can('create', Post::class)
             ]
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->user()->cannot('create', Post::class)) {
            abort(403);
        }
        $request->validate(['title' => 'required|min:2|max:50', 'body' => 'required|min:2']);
        
        $post = Post::create(array_merge($request->except('tags'), ['user_id' => Auth::id()]));
        
        $post->tags()->attach($request->tags);

        return Redirect::to("/");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        
        /* if ($request->header('tag') && $request->header('tag') != 'All') {
            $posts= Post::with('tags:name')->whereHas('tags', function (Builder $query) use ($request) {
                $query->where('name', '=', $request->header('tag'));
            })->orderBy('created_at', 'desc')->get();
        } else {
            
            $posts = Post::with('tags:name')->orderBy('created_at', 'desc')->get();
        } */
        if ($request->wantsJson()) {
            $post = Post::findOrFail($id);
            return response()->json(['post' => $post]);
        }
        return $this->index($request);
    
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        $post = Post::with('tags:id')->findOrFail($request->id);

        if ($request->user()->cannot('update', $post)) {
            abort(403);
        }
        
        $tags = Tag::all();
        return Inertia::render('Post/Edit', ['postToUpdate' => $post, 'allTags' => $tags]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post $post)
    {
        if ($request->user()->cannot('update', $post)) {
            abort(403);
        }
        
        $request->validate(['title' => 'required|min:2|max:50', 'body' => 'required|min:2']);
        $post = Post::findOrFail($request->id);
        $post->update(['title' => $request->title, 'body' => $request->body]);
        $post->tags()->sync($request->tags);
        
        return Redirect::to("/posts/$post->id");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(int $id)
    {
        Post::destroy($id);
        return Redirect::to("/");

    }
}

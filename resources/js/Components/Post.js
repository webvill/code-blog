import { Link } from "@inertiajs/inertia-react";
import CodeBlock from "./CodeBlock";

export default function Post({ post }) {
    return (
        <>
            <div className="d-flex justify-content-between border-0 border-start border-5 ps-1">
                <span className="lead fs-3">{post.title}</span>
                <Link
                    href={`/posts/${post.id}/edit`}
                    className="btn btn-primary"
                >
                    Redigera
                </Link>
            </div>
            <CodeBlock post={post} />
        </>
    );
}

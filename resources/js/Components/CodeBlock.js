import ReactMarkdown from "react-markdown";
import { Prism as SyntaxHighlighter } from "react-syntax-highlighter";
import {
    docco,
} from "react-syntax-highlighter/dist/esm/styles/prism";
import styles from "../../styles/markdown.module.css";

export default function CodeBlock({ post }) {
    return (
        <ReactMarkdown
            className={styles.markdown}
            children={post.body}
            components={{
                code({ node, inline, className, children, ...props }) {
                    const match = /language-(\w+)/.exec(className || "");
                    return !inline && match ? (
                        <SyntaxHighlighter
                            children={String(children).replace(/\n$/, "")}
                            style={docco}
                            /* customStyle={{
                                comment: {
                                    color: "#fff",
                                    backgroundColor: "blue",
                                },
                            }} */
                            language={match[1]}
                            PreTag="div"
                            {...props}
                        />
                    ) : (
                        <code className={className} {...props}>
                            {children}
                        </code>
                    );
                },
            }}
        />
    );
}
